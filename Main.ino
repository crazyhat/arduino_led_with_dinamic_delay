/**
 * Вход светодиода
 */
#define LED_PIN 13
/**
 * Шаг изменеия задержки
 */
#define STEP 50
/**
 * Порог задержки (максимальный)
 */
#define MAX_DELAY_THRESHOLD 500
/**
 * Порог задержки (минимальный)
 */
#define MIN_DELAY_THRESHOLD 50

/**
 * Задержка. Меняется с ходом выполнения программы
 */
int ledDelay = 500;
/**
 * Ход изменения задержки (вверх или вниз)
 */
boolean direction = true;

void setup() {
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  // мигание светодиода
  blinkLED();
  // проверяем направление и, при необходимости, меняем его
  setDirection();
  // меняем задерджку
  changeDirection();
}

/**
 * Метод заставляет мигать светодиод. Зависит от глобальных переменных.
 */
void blinkLED() {
  digitalWrite(LED_PIN, HIGH);
  delay(ledDelay);
  digitalWrite(LED_PIN, LOW);
  delay(ledDelay);
}

/**
 * Метод проверяет и меняет 
 */
void setDirection() {
  if (direction) {
    ledDelay += STEP;
  } else {
    ledDelay -= STEP;
  }
}

/**
 * Метод меняет задержку в нужном направлении
 */
void changeDirection() {
  if (ledDelay >= MAX_DELAY_THRESHOLD) {
    direction = false;
  } else if (ledDelay <= MIN_DELAY_THRESHOLD) {
    direction = true;
  }
}

